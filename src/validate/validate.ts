export function validate(n: number) {
  if (isNaN(n)) throw new Error(`input must be a number, got '${n}'`);
  if (n <= 0) throw new Error(`input must be greater than 0, got '${n}'`);
  if (n > 100) throw new Error(`input must be less than 100, got '${n}'`);
  return;
}
