import React, { ChangeEvent, useState } from 'react';
import classNames from 'classnames';

import './app.scss';
import { handleRequest, SquaresDiffResponse } from './handle-request/handle-request';
import { validate } from './validate/validate';

type LoadState = 'fetching' | 'error' | 'at-rest';

const LABEL_TEXT = `Enter a number greater than 0 and less than or equal to 100.`;
const PLACEHOLDER_TEXT = `If you ever wanted to query the difference between:
(a) the sum of the squares of the first n natural numbers and (b) the square of the sum of the first n natural numbers,
where n is an integer greater than 0 and less than or equal to 100, this app is for you.
`;

const App = () => {
  const [loadState, setLoadState] = useState<LoadState>('at-rest');
  const [submitError, setSubmitError] = useState<string | undefined>(undefined);
  const [response, setResponse] = useState<SquaresDiffResponse | undefined>(undefined);
  const [nInputValue, setNInputValue] = useState('');

  const onSubmit = async () => {
    setLoadState('fetching');
    let response: SquaresDiffResponse | undefined;
    try {
      response = await handleRequest({
        n: Number(nInputValue),
        type: 'computeSquaresDiff',
        latency: (window as any).fakeLatency,
      });
    } catch (e) {
      setLoadState('error');
      setSubmitError(e.message);
      setResponse(undefined);
      return;
    }

    setLoadState('at-rest');
    setSubmitError(undefined);
    setResponse(response);
  };

  const renderResults = () => {
    switch (loadState) {
      case 'fetching':
        return <div className="loader">Running through advanced algorithms...</div>;
      case 'error':
        return <div className="error">Error: {submitError || `An unexpected error occurred.`}</div>;
      case 'at-rest':
        if (!response) {
          return <div className="placeholder">{PLACEHOLDER_TEXT}</div>;
        }
        const { value, occurrences } = response;

        return (
          <div className="results">
            <div className="header">
              <div className="value">Result: {value}</div>
              <div className="metadata">
                <div>Total requests: {occurrences.length}</div>
              </div>
            </div>
            <div className="occurrences">
              {occurrences.map(occurrence => {
                const occurrenceStr = occurrence.toISOString();
                return (
                  <div key={occurrenceStr} className="occurrence">
                    <div className="label">{occurrenceStr}</div>
                  </div>
                );
              })}
            </div>
          </div>
        );
    }
  };

  let validationError: string | undefined;
  if (nInputValue !== '') {
    const n = Number(nInputValue);
    try {
      validate(n);
    } catch (e) {
      validationError = e.message;
    }
  }

  let buttonText: string;
  if (loadState === 'fetching') {
    buttonText = 'Calculating...';
  } else if (loadState === 'error') {
    buttonText = 'Retry';
  } else {
    buttonText = 'Go';
  }

  const submitDisabled = loadState === 'fetching' || validationError != null || nInputValue === '';

  return (
    <div className="app">
      <div className="center-content">
        <div className="form">
          <div className="label">{LABEL_TEXT}</div>
          <input
            className={classNames({
              'has-error': !!validationError,
            })}
            onChange={(event: ChangeEvent<HTMLInputElement>) => {
              const inputValue = (event.target as HTMLInputElement).value as string;
              setNInputValue(inputValue);
            }}
            onKeyPress={event => {
              if (event.key === 'Enter' && !submitDisabled) {
                onSubmit();
              }
            }}
          />

          <button
            disabled={submitDisabled}
            className={classNames('primary-button', { disabled: submitDisabled })}
            onClick={onSubmit}
          >
            {buttonText}
          </button>
          {validationError && <div className="validation-error">{validationError}</div>}
        </div>
        {renderResults()}
      </div>
    </div>
  );
};

export default App;
