import { computeSquaresDiff } from '../compute-squares-diff/compute-squares-diff';

// mock request handler
const sleep = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

const MAX_ENTRIES = 10000;
export interface Request {
  type: 'computeSquaresDiff';
  n: number;
  latency?: number;
}

export interface SquaresDiffResponse {
  datetime: Date;
  value: number;
  occurrences: Date[];
  last_datetime: Date;
}

let requests: Record<number, Date[]> = {};

function addOrUpdate(n: number, date: Date) {
  if (requests[n] != null) {
    const currentLength = requests[n].length;
    if (currentLength >= MAX_ENTRIES) {
      console.log(`Too many.. trimming`);
      requests[n] = requests[n].slice(0, MAX_ENTRIES);
    }

    requests[n] = [date].concat(requests[n]);
  } else {
    requests[n] = [date];
  }
}

function getOccurrences(n: number): Date[] {
  if (!requests[n]) {
    throw new Error(`Index ${n} has not been initialized.`);
  }
  return requests[n];
}

let computeSquaresDiffCache: Record<number, number> = {};

export async function handleRequest(request: Request): Promise<SquaresDiffResponse> {
  const { latency, type } = request;
  if (latency) {
    await sleep(latency);
  }

  if (type === 'computeSquaresDiff') {
    const { n } = request;
    const now = new Date();
    addOrUpdate(n, now);

    if (computeSquaresDiffCache[n] != null) {
      console.log(`Cache hit for ${n}`);
    } else {
      computeSquaresDiffCache[n] = computeSquaresDiff(n);
    }

    const diff = computeSquaresDiffCache[n];

    if (diff == null) {
      throw new Error(`Failed to compute..`);
    }

    const occurrences = getOccurrences(n);
    return Promise.resolve({
      datetime: now,
      value: diff,
      occurrences,
      last_datetime: occurrences.length > 1 ? occurrences[1] : occurrences[0],
    });
  }

  throw new Error(`Unknown request type: ${request.type}`);
}
