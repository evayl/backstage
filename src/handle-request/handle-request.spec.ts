import { handleRequest } from './handle-request';
const FIXED_DATE = new Date('2020-01-02');
const _myDate = Date;
(global as any).Date = jest.fn(() => FIXED_DATE);

function incrementDate(date: Date) {
  return new _myDate(date.valueOf() + 1);
}

describe('handleRequest', () => {
  it('.fails with too big of input', async () => {
    expect.assertions(1);
    try {
      await handleRequest({
        n: 8000,
        type: 'computeSquaresDiff',
      });
    } catch (e) {
      expect(e.message).toEqual(`input must be less than 100, got '8000'`);
    }
  });

  it('.fails with negative input', async () => {
    expect.assertions(1);
    try {
      await handleRequest({
        n: -5,
        type: 'computeSquaresDiff',
      });
    } catch (e) {
      expect(e.message).toEqual(`input must be greater than 0, got '-5'`);
    }
  });

  it('.properly increments', async () => {
    expect(
      await handleRequest({
        n: 80,
        type: 'computeSquaresDiff',
      }),
    ).toEqual({
      datetime: FIXED_DATE,
      last_datetime: FIXED_DATE,
      occurrences: [FIXED_DATE],
      value: 10323720,
    });

    const date2 = incrementDate(FIXED_DATE);
    (global as any).Date = jest.fn(() => date2);

    expect(
      await handleRequest({
        n: 80,
        type: 'computeSquaresDiff',
      }),
    ).toEqual({
      datetime: date2,
      last_datetime: FIXED_DATE,
      occurrences: [date2, FIXED_DATE],
      value: 10323720,
    });

    const date3 = incrementDate(date2);
    (global as any).Date = jest.fn(() => date3);

    expect(
      await handleRequest({
        n: 80,
        type: 'computeSquaresDiff',
      }),
    ).toEqual({
      datetime: date3,
      last_datetime: date2,
      occurrences: [date3, date2, FIXED_DATE],
      value: 10323720,
    });

    expect(
      await handleRequest({
        n: 2,
        type: 'computeSquaresDiff',
      }),
    ).toEqual({
      datetime: date3,
      last_datetime: date3,
      occurrences: [date3],
      value: 4,
    });
  });
});
