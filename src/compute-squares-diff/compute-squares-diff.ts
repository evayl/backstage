// compute the difference between:
// (a) the sum of the squares of the first n natural numbers and (b) the square of the sum of the first n
// natural numbers, where n is an integer greater than 0 and less than or equal to 100.

import { validate } from '../validate/validate';

// exported for tests
export function squareOfSum(n: number) {
  validate(n);
  const sum = (n * (n + 1)) / 2;
  return sum * sum;
}

// exported for tests
export function sumOfSquares(n: number) {
  // I remembered there was a formula for this and had to look it up.
  // If I couldn't, I would've probably brute forced it looping through 0... n
  validate(n);
  return (n * (n + 1) * (2 * n + 1)) / 6;
}

export function computeSquaresDiff(n: number) {
  validate(n);
  return Math.abs(sumOfSquares(n) - squareOfSum(n));
}
