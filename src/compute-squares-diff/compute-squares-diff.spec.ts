import { computeSquaresDiff, squareOfSum, sumOfSquares } from './compute-squares-diff';

describe('computeSquareDiff', () => {
  it('.computeSquareDiff works', () => {
    expect(computeSquaresDiff(10)).toEqual(2640);
    expect(computeSquaresDiff(20)).toEqual(41230);
    expect(computeSquaresDiff(1)).toEqual(0);
  });

  it('sumOfSquares works', () => {
    expect(sumOfSquares(10)).toEqual(385);
    expect(sumOfSquares(20)).toEqual(2870);
    expect(sumOfSquares(1)).toEqual(1);
  });

  it('squareOfSum works', () => {
    expect(squareOfSum(10)).toEqual(3025);
    expect(squareOfSum(20)).toEqual(44100);
    expect(squareOfSum(1)).toEqual(1);
  });
});
